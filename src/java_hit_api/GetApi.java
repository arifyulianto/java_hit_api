/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package java_hit_api;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author VOLD
 */
public class GetApi {
    
    public static void main(String[] args) throws ParserConfigurationException, SAXException {
        // TODO code application logic here
        
        
        JsonParser parser = new JsonParser();

        String json = "{\"data\":{\"trx\":{\"trx_id\":\"4013298675\",\"trx_type\":\"2100\",\"product_type\":\"BPJS-KESEHATAN\",\"stan\":\"257095\",\"amount\":\"242500\",\"datetime\":\"20190424095138\",\"merchant_code\":\"6012\",\"rc\":\"0000\",\"admin_charge\":\"2500\",\"no_va\":\"0000002426690259  \",\"periode\":\"03\",\"name\":\"OIEJ HAIJ LIEN       (PST:  1)\",\"kode_cabang\":\"1101  \",\"nama_cabang\":\"SEMARANG                                                                                            \",\"premi\":\"240000\",\"sisa\":\"000000000000\"}}}";

        JsonElement jsonTree = parser.parse(json);

        if(jsonTree.isJsonObject()){
            JsonObject jsonObject = jsonTree.getAsJsonObject();

            JsonElement data = jsonObject.get("data");
            
            if (data.isJsonObject()) {
                JsonObject trxObj = data.getAsJsonObject();
                
                JsonElement trx = trxObj.get("trx");
                
                if (trx.isJsonObject()) {
                    JsonObject d = trx.getAsJsonObject();
                        
                    JsonElement trxid = d.get("trx_id");
                    JsonElement product = d.get("product_type");
                    JsonElement amount = d.get("amount");
                    JsonElement name = d.get("name");
                    JsonElement nc = d.get("nama_cabang");
                    JsonElement s = d.get("sisa");
                    
                    System.out.println("Trx ID: " + trxid.toString().replaceAll("\"", ""));
                    System.out.println("Product Type: " + product.toString().replaceAll("\"", ""));
                    System.out.println("Amount: " + amount.toString().replaceAll("\"", ""));
                    System.out.println("Name: " + name.toString().replaceAll("\"", ""));
                    System.out.println("Cabang: " + nc.toString().replaceAll("\"", ""));
                    System.out.println("Sisa: " + s.toString().replaceAll("\"", ""));
                }
            }

        }
        
        System.out.println("====================");
        
        try {
            GetApiXml();
        } catch (IOException ex) {
            Logger.getLogger(GetApi.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    
    public static void GetApiXml() throws ParserConfigurationException, SAXException, IOException {
        
        String xml = "<response><product_type>PDAM</product_type><mti>0210</mti><pan>074035</pan><processing_code>380000</processing_code><amount>000000132600</amount><transmission_date_time>0104135211</transmission_date_time><stan>000192235147</stan><local_trx_time>135211</local_trx_time><local_trx_date>0104</local_trx_date><settlement_date>0105</settlement_date><merchant_type>6012</merchant_type><acquiring_institution_id>008</acquiring_institution_id><retrieval_ref_no>000192235147</retrieval_ref_no><rc>00</rc><terminal_id>DEVELPLG</terminal_id><acceptor_identification_code>200900100800000</acceptor_identification_code><private_data_48>02160130121 201811201812AGENG GIRIYONO 0202127600 000050002018110000001276000000000000000007-000000002018120000001276000000000000000007-00000000</private_data_48><idpel>02160130121 </idpel><blth>201811201812</blth><name>AGENG GIRIYONO </name><bill_count>02</bill_count><bill_repeat_count>02</bill_repeat_count><rp_tag>127600 </rp_tag><biaya_admin>00005000</biaya_admin><bills><bill><bill_date>201811</bill_date><bill_amount>000000127600</bill_amount><penalty>00000000</penalty><kubikasi>00000007-00000000</kubikasi></bill><bill><bill_date>201812</bill_date><bill_amount>000000127600</bill_amount><penalty>00000000</penalty><kubikasi>00000007-00000000</kubikasi></bill></bills></response>";
        
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        InputSource src = new InputSource();
        src.setCharacterStream(new StringReader(xml));

        Document doc = builder.parse(src);
        String bd = doc.getElementsByTagName("bill_date").item(0).getTextContent();
        String ba = doc.getElementsByTagName("bill_amount").item(0).getTextContent();
        String kb = doc.getElementsByTagName("kubikasi").item(0).getTextContent();
        
        System.out.println(bd);
        System.out.println(ba);
        System.out.println(kb);
        
    }
    
}
